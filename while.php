<?php

/* example 1 */

$i = 1;
while ($i <= 10) {
    echo $i++;
    echo "</br>";/* the printed value would be
                   $i before the increment
                   (post-increment) */
}
